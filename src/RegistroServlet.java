

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


@WebServlet("/rs")
public class RegistroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
    	try {
			InitialContext context=new InitialContext();
			ds=(DataSource) context.lookup("java:comp/env/jdbc/ge");
			
			if(ds==null) {
				throw new ServletException("DataSource desconocido.");
			}
		} catch (NamingException e) {
			Logger.getLogger(AltaIngenieros.class.getName()).log(Level.SEVERE,null,e);
			
		}
    }   
    
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String[]> frm = request.getParameterMap();
    	if (frm.isEmpty()) {
    		enviarFormulario(response);
    	}else {
    		
    	}
		
	}
	protected void enviarFormulario(HttpServletResponse response) throws IOException {
		StringBuilder contenido = new StringBuilder();
		contenido.append("<header>\n");
		contenido.append("	<h1>Registrar nuevo usuario</h1>\n");	
		contenido.append("</header>\n");
		contenido.append("<div class=\"clear\"></div>\n");
		contenido.append("<form action=\"rs\" method=\"post\" >\n");
		contenido.append("	<div id=\"out\"><div class=\"out\">\n");
		contenido.append("		<div class=\"in\">\n");
		contenido.append("			<p><label for=\"nss\" id=\"nsslbl\">Número de la Seguridad Social</label></p>\n");
		contenido.append("			<p><input type=\"text\" id=\"nss\" name=\"nss\" oninput=\"if (error) limpiar('nss')\"/></p>\n");
		contenido.append("			<p><label for=\"nombre\" id=\"nombrelbl\">Nombre</label></p>\n");
		contenido.append("			<p><input type=\"text\" id=\"nombre\" name=\"nombre\" oninput=\"if (error) limpiar('nombre')\"/></p>\n");
		contenido.append("			<p><label for=\"salario\" id=\"salariolbl\">Salario</label></p>\n");
		contenido.append("			<p><input type=\"text\" id=\"salario\" name=\"salario\" oninput=\"if (error) limpiar('salario')\"/></p>\n");
		contenido.append("		</div>\n");
		contenido.append("	</div>\n");
		contenido.append("	<p id=\"enviar\"><input type=\"submit\" value=\"Enviar\" /></p>\n");
		contenido.append("</form>\n");
		HTML5.enviarPagina(response, contenido.toString(), "Altas de ingenieros", "frmingeniero", "frmingeniero");
	}
	 protected void altaEmpleado(HttpServletResponse response, Map<String, String[]> frm) throws IOException {
    	Connection connection =null;
    	String sql = null;
    	try {
    		
			String nss = frm.get("nss")[0];
			String nombre = frm.get("nombre")[0];
			
			
			connection = ds.getConnection();
			Statement statement = connection.createStatement();

			sql = String.format("insert into empleados values (%s, '%s', %s)", nss, nombre);
			statement.executeUpdate(sql);
			
			//Se deben hacer todas las inserciones en una sola transacción
			
			response.sendRedirect("rs");
		} catch (SQLException e) {
			//No generar este error en entorno de produccion, usar Logger.getLogger()
			StringBuilder contenido = new StringBuilder();
			contenido.append("<p>Se ha producido un error al intentar actualizar la base de datos:</p>");
			if (sql != null) {
				contenido.append("<p>Sentencia: ");
				contenido.append(sql);
				contenido.append("</p>");
			}
			contenido.append("<p>" + e.getLocalizedMessage() + "</p>");
			contenido.append("<p><a href=\"rs\">Retornar al formulario</a></p>");
			HTML5.enviarPagina(response, contenido.toString(), "Error", null, null);
		} finally {	
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
				}
		}
		
    }

}
