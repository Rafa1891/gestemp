

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/ni")
public class NuevoIngeniero extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public NuevoIngeniero() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");	
		PrintWriter out=response.getWriter();
		HTML5.comienzo(out, "Gestión de Empleados");
		out.println("<form method=\"get\" action=\"/ni\"> ");
		out.println("<p><label for=\"ing\">Nuevo Ingeniero</label></p>");
		out.println("Número de la SS:<input type=\"number\" id=\"nss\"></br>");
		out.println("Departamento:<input type=\"text\" id=\"departamento\"></br>\"");
		out.println("<input type=\"submit\" id=\"enviar\">");
		out.println("</form>");
		out.println("<script>");
		out.println("var nss=document.getElementById('nss').value");
		out.println("var dep=document.getElementById('departamento').value");
		out.println("</script>");
		agregarIngeniero(out);
		HTML5.fin(out);
		
	}
    
	private void agregarIngeniero(PrintWriter out) {
		try {
		
		Connection cn=null; 
		cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados","rafa","rafa");
		Statement st=cn.createStatement();
		String insercion=("INSERT INTO ingenieros VALUES(?,?)");

			      
			      PreparedStatement preparedStmt = cn.prepareStatement(insercion);
			      preparedStmt.setString (1, "%s");
			      preparedStmt.setString (2, "%s");
		
			      preparedStmt.execute();
		cn.close();
		}catch(SQLException s) {
			System.out.println("ERROR: "+s.getMessage());
		}
	}
	
	

}
