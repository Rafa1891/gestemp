import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class CifradoContrasenia {

	public static void main(String[] args) {
		String password = "practicas";
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 0xffff, 128);

		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = factory.generateSecret(spec).getEncoded();
			String passwordHash = Base64.getEncoder().encodeToString(hash);
			//+Base64.getEncoder().encodeToString(salt) .Unir el salt a la contraseña cifrada, .getDecoder() para descodificar
			System.out.println(passwordHash);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();

		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

	}

}
