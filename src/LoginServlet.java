

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/ls")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public LoginServlet() {
        super();
        
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Map<String, String[]> frm = request.getParameterMap();
    	if (frm.isEmpty()) {
    		enviarFormulario(response);
    	}else {
    		
    		String usuario = frm.get("usuario")[0];
		String contrasenia = frm.get("contrasenia")[0];
		HttpSession sesion=request.getSession();
    	usuario=(String) sesion.getAttribute("usuario");//Crear variable de sesion
    	sesion.setAttribute("usuario", usuario);
    	sesion.setAttribute("contrasenia", contrasenia);
    	}
	}
	protected void enviarFormulario(HttpServletResponse response) throws IOException {
		StringBuilder contenido = new StringBuilder();
		contenido.append("<header>\n");
		contenido.append("	<h1>Iniciar sesión</h1>\n");
		contenido.append("</header>\n");
		contenido.append("<div class=\"clear\"></div>\n");
		contenido.append("<form action=\"ls\" method=\"post\" >\n");
		contenido.append("	<div id=\"out\"><div class=\"out\">\n");
		contenido.append("		<div class=\"in\">\n");
		contenido.append("			<p><label for=\"usuario\" id=\"usuariolbl\">Usuario</label></p>\n");
		contenido.append("			<p><input type=\"text\" id=\"usuario\" name=\"usuario\" oninput=\"if (error) limpiar('usuario')\"/></p>\n");
		contenido.append("			<p><label for=\"contrasenia\" id=\"contrasenialbl\">Contraseña</label></p>\n");
		contenido.append("			<p><input type=\"text\" id=\"contrasenia\" name=\"contrasenia\" oninput=\"if (error) limpiar('contrasenia')\"/></p>\n");		
		contenido.append("		</div>\n");
		contenido.append("	</div>\n");		
		contenido.append("	<p id=\"enviar\"><input type=\"submit\" value=\"Enviar\" /></p>\n");
		contenido.append("</form>\n");
		HTML5.enviarPagina(response, contenido.toString(), "Login", "ls", "ls");
	}
	

}
