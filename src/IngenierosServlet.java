

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/gi")
public class IngenierosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public IngenierosServlet() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");	
		PrintWriter out=response.getWriter();
		HTML5.comienzo(out, "Gestión de Empleados");
		out.println("<form method=\"get\" action=\"/gi\"> ");
		out.println("<p><label for=\"nss\">Número de la SS:</label></p>");
		out.println("<input type=\"number\" id=\"nss\"></br>");
		out.println("<p><label for=\"dep\">Nombre:</label></p>");
		out.println("<input type=\"text\" id=\"nombre\"></br>");
		out.println("<p><label for=\"nss\">Salario:</label></p>");
		out.println("<input type=\"text\" id=\"salario\"></br>");
		out.println("<p><label for=\"dep\">Departamento:</label></p>");
		out.println("<select name=\"dep\">");
		agregarDepartamentos(out);
		out.println("</select></br>");
		out.println("<input type=\"submit\" id=\"enviar\">");
		out.println("</form>");
		HTML5.fin(out);
	}
	private void agregarDepartamentos(PrintWriter out) {
		try {
		
		Connection cn=null; 
		cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados","rafa","rafa");
		Statement st=cn.createStatement();
		ResultSet rs=st.executeQuery("SELECT nombre FROM departamentos");
		
		while(rs.next()) {
			out.printf("<option>%s</option>", rs.getString("nombre"));
		}
		rs.close();
		cn.close();
		}catch(SQLException s) {
			System.out.println("ERROR: "+s.getMessage());
		}
	}
	private void agregarIngeniero(PrintWriter out) {
		try {
		
		Connection cn=null; 
		cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados","rafa","rafa");
		Statement st=cn.createStatement();
		String insercion=("INSERT INTO ingenieros VALUES(?,?)");

			      
			      PreparedStatement preparedStmt = cn.prepareStatement(insercion);
			      preparedStmt.setInt (1, 4);
			      preparedStmt.setString (2, "%s");
		
			      preparedStmt.execute();
		cn.close();
		}catch(SQLException s) {
			System.out.println("ERROR: "+s.getMessage());
		}
	}

	
	

}
